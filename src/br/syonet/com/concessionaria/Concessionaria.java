package br.syonet.com.concessionaria;

public class Concessionaria extends Veiculo {

	public TipoClienteEnum tipocliente;

	public Concessionaria(int incluiveiculo, int quantrodas, double peso, String cor, int ano, String marca,
			ModeloEnum modelo, TipoVeiculoEnum tipoveiculo, double valorvenda, TipoClienteEnum tipocliente) {
		super(incluiveiculo, quantrodas, peso, cor, ano, marca, modelo, tipoveiculo, valorvenda, tipocliente);
		// TODO Auto-generated constructor stub
	}

	public int incluindoVeiculo() {
		return this.incluiveiculo += 1;
	}
	
	public double alteraValor() {
		return this.valorvenda;
	}

	public  ModeloEnum  selecionaTipo(ModeloEnum ModeloEnum) {
		if(ModeloEnum == modelo.NOVO) {
			System.out.println("Voc� escolheu os modelos Novos!");	
		} if(ModeloEnum == modelo.SEMINOVO) {
			System.out.println("Voc� escolheu os modelos Seminovos!");
		} if(ModeloEnum == modelo.AMBOS) {
			System.out.println("Voc� deseja visuaizar ambos modelos!");
		}
		return modelo;
	}
	
	public TipoClienteEnum selecionaCliente(TipoClienteEnum tipoCliente) {
		if(tipoCliente == tipoCliente.PCD) {
		 System.out.println("Cliente com 50% de desconto!");
		} if(tipoCliente == tipoCliente.AGRICULTOR) {
			 System.out.println("Cliente com 40% de desconto!");
			} 
		return tipocliente;
	}
	
	public String toString() {
		return "Inclui ve�culo:\n" + getIncluiveiculo() + "Quantidade de rodas:\n" + getQuantrodas() + "Peso:\n" + getPeso() +
				"Cor:\n" + getCor() + "Ano:\n" + getAno() + "Marca:\n" + getMarca() + "Modelo" + getModelo() + "Tipo Veicuo:\n" + getTipoveiculo() +
				"Vaor de venda:\n" + getValorvenda() + "Tipo de Cliente:\n" + getTipocliente();
	}
	
}
