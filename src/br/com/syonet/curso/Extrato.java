//*Explica��o da execu��o do c�digo:o c�digo ir� realizar a soma do total do primeiro trimestre,
//ap�s isto, ir� realizar a divis�o perante os 3 meses para sabermos � m�dia mensal;
//@author Louise Tormes

package br.com.syonet.curso;

public class Extrato {

	public static void main(String[] args) {
		float janeiro = 15.000f;
		float fevereiro = 23.000f;
		float marco = 17.000f;

		float totaltrimestre = janeiro + fevereiro + marco;

		float media = (janeiro + fevereiro + marco) / 3;

		System.out.println("Despesa total do 1� trimestre:\n" + totaltrimestre);
		System.out.println("M�dia mensal de gastos do 1� trimestre:\n" + media);

	}

}
