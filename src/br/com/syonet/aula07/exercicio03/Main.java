package br.com.syonet.aula07.exercicio03;


	
import java.util.Arrays;
import java.util.List;

	public class Main {

		public static void main(String[] args) {
			DadosPessoa primeiraPessoa = new Pessoa()
					.nome("Louise")
					.dataNascimento("04/05/1996")
					.cidade("Est�ncia Velha")
					.buid();
			
			
			DadosPessoa segundaPessoa = new Pessoa()
					.nome("Lucas")
					.dataNascimento("06/06/1994")
					.cidade("Port�o")
					.buid();
			
			DadosPessoa terceiraPessoa = new Pessoa()
					.nome("Pedro")
					.dataNascimento("15/03/2016")
					.cidade("S�o Leopoldo")
					.buid();
			
			DadosPessoa quartaPessoa = new Pessoa()
					.nome("Theo")
					.dataNascimento("02/03/2022")
					.cidade("Novo Hamburgo")
					.buid();
			
			
			List<DadosPessoa> listaPessoas =  Arrays.asList(primeiraPessoa,segundaPessoa,terceiraPessoa,quartaPessoa);
			
			listaPessoas.forEach(System.out::println);
			
			
			listaPessoas =  DadosPessoa.pessoasMaiorDeIdade(listaPessoas);
			
			System.out.println("Maiores de Idade:\n");
			
			listaPessoas.forEach(System.out::println);
		}

	}



