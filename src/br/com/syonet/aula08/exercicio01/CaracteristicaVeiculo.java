package br.com.syonet.aula08.exercicio01;

public class CaracteristicaVeiculo {


		private String marca;
		private String modelo;
		private String placa;
		private String chassi;
		private double valor;

		public CaracteristicaVeiculo(String marca, String modelo, String placa, String chassi, double valor) {
			this.marca = marca;
			this.modelo = modelo;
			this.placa = placa;
			this.chassi = chassi;
			this.valor = valor;
		}
		
		public String getMarca() {
			return marca;
		}
		
		public void setMarca(String marca) {
			this.marca = marca;
		}
		
		public String getModelo() {
			return modelo;
		}
		
		public void setModelo(String modelo) {
			this.modelo = modelo;
		}
		
		public String getPlaca() {
			return placa;
		}
		
		public void setPlaca(String placa) {
			this.placa = placa;
		}
		
		public String getChassi() {
			return chassi;
		}
		
		public void setChassi(String chassi) {
			this.chassi = chassi;
		}
		
		public double getValor() {
			return valor;
		}
		
		public void setValor(double valor) {
			this.valor = valor;
		}
		
		
		public String toString() {
			return  
		"Marca: " + marca +  
		"\nModelo: " + modelo + 
		"\nPlaca: " + placa + 
		"\nChassi: " + chassi + 
		"\nR$: " + valor +
		"\n----------\n";
		}

		public int compareTo(CaracteristicaVeiculo listaVeiculo) {
			if (this.valor > listaVeiculo.valor) {
				return -1;
			}
			if (this.valor < listaVeiculo.valor) {
				return 1;
			}
			return 0;
		}
		
		
	}


	

