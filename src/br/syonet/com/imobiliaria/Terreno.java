package br.syonet.com.imobiliaria;

public class Terreno extends Imovel {
	
	public Terreno(float valorvenda, float areaimovel) {
		super(valorvenda, areaimovel);
		
	}

	public String toString() {
		return "Terreno: " + "\nArea Imovel: " + getAreaimovel() + "\nValor do Imovel: R$ " + getValorvenda();
		}
}
