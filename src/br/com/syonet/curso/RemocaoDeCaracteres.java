//*Explica��o da execu��o do c�digo:O c�digo ir� verificar as letras vogais da palavra informada 
//pelo usu�rio, ap�s isto ir� remove-las e retornar o resultado da palavra sem as vogais.
//Para chegar neste c�digo realizei a inclus�o do m�todo de entrada Scanner, que ir� fazer a
//leitura da palavra e com o m�todo replace iremos identificar essas letras vogais e remove-las,
//e ent�o iremos imprimir a palavra na tela sem as vogais.
//@author Louise Tormes

package br.com.syonet.curso;

import java.util.Scanner;

public class RemocaoDeCaracteres {
	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite a palavra que deseja remover as vogais:\n");
		String palavra = entrada.next();
		palavra = palavra.replaceAll("[A E I O U a e i o u]", "");

		System.out.println("Vogais removidas:\n" + palavra);

		entrada.close();

	}
}
