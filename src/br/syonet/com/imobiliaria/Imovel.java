package br.syonet.com.imobiliaria;

public class Imovel {

	public float valorvenda;

	public float areaimovel;

	public Imovel() {
		super();
	}

	public Imovel(float valorvenda, float areaimovel) {
		this.valorvenda = valorvenda;
		this.areaimovel = areaimovel;
	}

	public float getValorvenda() {
		return valorvenda;
	}

	public void setValorvenda(float valorvenda) {
		this.valorvenda = valorvenda;
	}

	public float getAreaimovel() {
		return areaimovel;
	}

	public void setAreaimovel(float areaimovel) {
		this.areaimovel = areaimovel;
	}

	public String toString() {
		return "Valor da venda: " + getValorvenda() + "Area do im�vel: " + getAreaimovel();
	}
}
