package br.syonet.com.mensagens;

//Exercicio Mensagens: para resolu��o deste exerc�cio criei primeiramente a classe Contato,
//na qual fiz exten��o de heran�a nas demais, criei tamb�m os m�todos conforme solicitado no exercicio,
//e suas vari�veis.Para a listagem das informa��es de forma correta, utilizei um arraylist
//juntamente com um for each para a impress�o dos dados.

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Contato> listaMensagens = new ArrayList<>();
		Sms s = new Sms("Sms");
		Email e = new Email("Email", "Nome");

		s.setNome("Louise Tormes");
		s.setMensagem("Bom dia, como vai?");
		s.setTelefone("51 997652839");

		e.setEmaildestinatario("louise.tormes@syonet.com");
		e.setEmailremetente("maria@outlook.com");
		e.setNomedestinatario("Louise Tormes");
		e.setNome("Maria");
		e.setMensagem("Boa tarde, gostaria de confirmar nossa reuni�o para amanh�.");

		listaMensagens.add(s);
		listaMensagens.add(e);

		for (Contato listaMensagem : listaMensagens) {
			System.out.println(listaMensagem);
		}
	}
}
