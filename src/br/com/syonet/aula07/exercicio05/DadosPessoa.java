package br.com.syonet.aula07.exercicio05;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;



public class DadosPessoa {

	private String nome;
	private LocalDate dataNascimento;
	private String cidade;
	private Integer idade;
	private DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public DadosPessoa(String nome, String dataNascimento, String cidade) {
		super();
		this.nome = nome;
		this.dataNascimento = LocalDate.parse(dataNascimento, sdf);
		calculaIdade(LocalDate.parse(dataNascimento, sdf));
		this.cidade = cidade;
	}

	public Integer getIdade() {
		return idade;
	}
	
	public String getDataNascimento() {
		return  dataNascimento.format(sdf);
	}

	
	public String toString() {
		return "Nome: " + nome  + "\nData Nascimento: " + getDataNascimento() +  "\nCidade: " + cidade 
				+ "\nIdade: " + idade + "\n";
				

	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = LocalDate.parse(dataNascimento, sdf);
		calculaIdade(this.dataNascimento);
	}

	public void calculaIdade(LocalDate dataNascimento) {
		LocalDate dataAtual = LocalDate.now();

		if (dataAtual.getMonth().getValue() > dataNascimento.getMonthValue()) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
			return;
		}
		if (dataAtual.getMonth().getValue() == dataNascimento.getMonthValue()
				&& dataAtual.getDayOfMonth() >= dataNascimento.getDayOfMonth()) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
		} else {
			this.idade = dataAtual.getYear() - dataNascimento.getYear() - 1;
		}
	}
	
	public static List<DadosPessoa> pessoasMaiorDeIdade(List<DadosPessoa> lista){
		List<DadosPessoa> listaMaiorIdade = lista.stream().filter(p -> p.idade >= 18)
				.collect(Collectors.toList());
		return listaMaiorIdade;
	}
	

}

