package br.syonet.com.imobiliaria;

public class Apartamento extends Imovel {

	public boolean salaofestas = true;
	public int quantcomodos;

	public Apartamento(float valorvenda, float areaimovel) {
		super(valorvenda, areaimovel);
	}

	public Apartamento(boolean salaofestas) {
		this.salaofestas = salaofestas;
	}

	public boolean getSalaofestas() {
		return salaofestas;

	}

	public void setSalaofestas(boolean salaofestas) {
		this.salaofestas = salaofestas;
	}

	public int getQuantcomodos() {
		return quantcomodos;
	}

	public void setQuantcomodos(int quantcomodos) {
		this.quantcomodos = quantcomodos;
	}

	public String toString() {
		return "Apartamento: " + "\nValor da venda: R$  " + getValorvenda() + "\n�rea do im�vel: " + getAreaimovel()
				+ "\nQuantidade de c�modos: " + getQuantcomodos() + "\nSalao de festas: " + getSalaofestas();
	}

}
