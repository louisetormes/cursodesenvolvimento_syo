package br.syonet.com.imobiliaria;

public class Casa extends Imovel {

	public float areaterreno;
	public int quantcomodos;

	public Casa(float valorvenda, float areaimovel) {
		super(valorvenda, areaimovel);
	}

	public Casa(float areaterreno) {
		this.areaterreno = areaterreno;
	}

	public float getAreaterreno() {
		return areaterreno;
	}

	public void setAreaterreno(float areaterreno) {
		this.areaterreno = areaterreno;
	}

	public int getQuantcomodos() {
		return quantcomodos;
	}

	public void setQuantcomodos(int quantcomodos) {
		this.quantcomodos = quantcomodos;
	}

	public String toString() {
		return "Casa: " + "\nArea do imovel: " + getAreaimovel() + "\nValor do imovel: R$ " + getValorvenda()
				+ "\nComodos: " + getQuantcomodos() + "\nArea do terreno: " + getAreaterreno();
	}
}