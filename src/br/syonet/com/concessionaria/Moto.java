package br.syonet.com.concessionaria;

public class Moto extends Veiculo {
	
	public Moto(int incluiveiculo, int quantrodas, double peso, String cor, int ano, String marca, ModeloEnum modelo,
			TipoVeiculoEnum tipoveiculo, double valorvenda, TipoClienteEnum tipocliente) {
		super(incluiveiculo, quantrodas, peso, cor, ano, marca, modelo, tipoveiculo, valorvenda, tipocliente);
		// TODO Auto-generated constructor stub
	}

	public int cilindradas;


	

	public int getCilindradas() {
		return cilindradas;
	}
	
	public void setCilindradas(int cilindradas) {
		this.cilindradas = cilindradas;
	}
	
	public int selecionaCilindradas() {
		return this.cilindradas;
	}
	
	public String motoporMarca() {
		return this.marca;
	}
	
}
