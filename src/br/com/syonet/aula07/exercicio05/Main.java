package br.com.syonet.aula07.exercicio05;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		DadosPessoa primeiraPessoa = new Pessoa()
				.nome("Louise")
				.sobrenome("Tormes")
				.dataNascimento("04/05/1996")
				.cidade("Port�o")
				.uf(UfEnum.RS);
		
				
		
		
		DadosPessoa segundaPessoa = new Pessoa()
				.nome("Lucas")
				.sobrenome("Reis")
				.dataNascimento("06/06/1994")
				.cidade("Joinville")
				.uf(UfEnum.SC);
				
	
		DadosPessoa terceiraPessoa = new Pessoa()
				.nome("Pedro")
				.sobrenome("Lucas")
				.dataNascimento("15/03/2016")
				.cidade("S�o Leopoldo")
				.uf(UfEnum.RS);
				
		
		
		
		List<DadosPessoa> listaPessoas= Arrays.asList(primeiraPessoa, segundaPessoa,terceiraPessoa);
		
		listaPessoas.forEach(System.out::println);
		
		System.out.println("Procura por estado\n:");
		
		List<DadosPessoa> listaPessoasEstado = Pessoa.buscarPorEstado(listaPessoas, UfEnum.SP);
		
		listaPessoasEstado.forEach(System.out::println);
		

	}
}

