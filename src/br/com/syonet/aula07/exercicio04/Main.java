package br.com.syonet.aula07.exercicio04;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {

	public static void main(String[] args) {
		DadosVeiculo primeiroVeiculo = new Veiculo()
				.marca("FORD")
				.modelo("KA")
				.chassi("WYH19965TU")
				.buid();
		
		
		DadosVeiculo segundoVeiculo = new Veiculo()
				.marca("CITROEN")
				.modelo("AIR CROSS")
				.chassi("QWTY544422TR")
				.buid();
		
		
		DadosVeiculo terceiroVeiculo = new Veiculo()
				.marca("CITROEN")
				.modelo("DS5")
				.chassi("JKL72245FT")
				.buid();
		
		List<DadosVeiculo> veiculos = Arrays.asList(primeiroVeiculo,segundoVeiculo, terceiroVeiculo);
		
		
		System.out.println("Procurando ve�culo");
		Optional<DadosVeiculo> veiculoChassi = DadosVeiculo.encontrarVeiculoChassi(veiculos, "WYH19965TU");
		
		if(veiculoChassi.isPresent()) {
			System.out.printf("Informa��o do ve�culo %s " +veiculoChassi.get(), veiculoChassi.get().getChassi().get() );
		}
		else {
			System.out.println("Ve�culo n�o encontrado na base!");
		}
		
		}

}
	

