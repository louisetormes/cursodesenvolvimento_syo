package br.syonet.com.elevador;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Elevador listElev[] = new Elevador[1];
		Elevador elevador = new Elevador(0, 0, 0, 0);
		listElev[0] = new Elevador(0, 0, 0, 0);
		int quantpessoas = 0;
		
		System.out.println("Capacidade total do Elevador:\n");
		elevador.capacidadetotal = entrada.nextInt();
		System.out.println("Total de Andares:\n");
		elevador.totalandares = entrada.nextInt();
		listElev[0].inicializa(0, 0);
		System.out.println("Quantidade de pessoas atualmente:\n");
		quantpessoas = entrada.nextInt();
		
		if(listElev[0].getAndaratual()<=listElev[0].getTotalandares()) {
			listElev[0].sobeAndar();
			System.out.println("Subindo!");
		} else {
			System.out.println("Ultrapassou o andar m�ximo!");
		}
		
		if(listElev[0].getAndaratual()>=0) {
			listElev[0].desceAndar();
			System.out.println("Descendo!");
		} else {
			System.out.println("Andar m�nimo n�o atingido!");
		}
		
		if(listElev[0].getPessoas()<=listElev[0].getCapacidadetotal()) {
			listElev[0].entraPessoa();
			System.out.println("Entrada permitida!");
		} else {
			System.out.println("Quantidade de pessoas excedida!");
		}
		
		if(listElev[0].getPessoas()>0) {
			listElev[0].saiPessoa();
			System.out.println("Saindo!");
		} else {
			System.out.println("N�o � pessoas para sair!");
		}
		
		System.out.println("Quantidade de pessoas:\n" + listElev[0].getPessoas());
		System.out.println("Capacidade m�xima:\n" + listElev[0].getCapacidadetotal());
		System.out.println("Andar atual:\n" + listElev[0].getAndaratual());
		System.out.println("Quantidade de andares:\n" + listElev[0].getTotalandares());
		
		
		
			
		
			
		
	}

}
