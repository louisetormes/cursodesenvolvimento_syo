package br.com.syonet.aula07.exercicio03;


	public class Pessoa {

		private String nome;
		private String dataNascimento;
		private String cidade;

		public Pessoa nome(String nome) {
			this.nome = nome;
			return this;
		}

		public Pessoa dataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
			return this;
		}

		public Pessoa cidade(String cidade) {
			this.cidade = cidade;
			return this;
		}

		public DadosPessoa buid() {
			return new DadosPessoa(nome, dataNascimento, cidade);
		}

	}


