package br.syonet.com.concessionaria;

public class Caminhao extends Veiculo {



	public Caminhao(int incluiveiculo, int quantrodas, double peso, String cor, int ano, String marca,
			ModeloEnum modelo, TipoVeiculoEnum tipoveiculo, double valorvenda, TipoClienteEnum tipocliente) {
		super(incluiveiculo, quantrodas, peso, cor, ano, marca, modelo, tipoveiculo, valorvenda, tipocliente);
		// TODO Auto-generated constructor stub
	}

	public TipoCarroceriaEnum tipocarroceria;
	
	
	public TipoCarroceriaEnum getTipocarroceria() {
		return tipocarroceria;
	}
	
	public void setTipocarroceria(TipoCarroceriaEnum tipocarroceria) {
		this.tipocarroceria = tipocarroceria;
	}
	
	public TipoCarroceriaEnum  selecionaCarroceria(TipoCarroceriaEnum TipoCarroceria) {
		if(TipoCarroceria == tipocarroceria.GRANELEIRO) {
			System.out.println("Voc� escolheu a carroceria Graneleiro!");	
		} if(TipoCarroceria == tipocarroceria.BAU) {
			System.out.println("Voc� escolheu a carroceria Ba�!");
		} if(TipoCarroceria == tipocarroceria.BAUFRIGORIFICO) {
			System.out.println("Voc� escolheu a carroceria Ba�-Frigor�fico!");
		} if(TipoCarroceria == tipocarroceria.PLATAFORMA) {
			System.out.println("Voc� escolheu a carroceria Plataforma!");
		} if(TipoCarroceria == tipocarroceria.TANQUE) {
			System.out.println("Voc� escolheu a carroceria Tanque!");
		} if(TipoCarroceria == tipocarroceria.CACAMBA) {
			System.out.println("Voc� escolheu a carroceria Ca�amba!");
		} 
		return tipocarroceria;
	}

}
