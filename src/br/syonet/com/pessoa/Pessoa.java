package br.syonet.com.pessoa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Pessoa {

	private static String nome;
	private static String dtnascimento;
	private static double altura;

	public Pessoa(String nome, String dtnascimento, double altura) {
		super();
		this.nome = nome;
		this.dtnascimento = dtnascimento;
		this.altura = altura;

	}

	public static void main(String[] args) throws ParseException {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Bem vindo(a)!\n");
		System.out.println("Nome: ");
		nome = entrada.next();
		System.out.println("\nData de nascimento: ");
		dtnascimento = entrada.next();
		System.out.println("\nAltura: ");
		altura = entrada.nextDouble();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse(dtnascimento);
		int age = calculaIdade(data);
		// A idade �:
		System.out.println("\nA idade �:\n" + age);

	}

	public static int calculaIdade(java.util.Date data) {

		Calendar dtnascimento = Calendar.getInstance();
		dtnascimento.setTime(data);
		Calendar atual = Calendar.getInstance();

		int age = atual.get(Calendar.YEAR) - dtnascimento.get(Calendar.YEAR);

		if (atual.get(Calendar.MONTH) < dtnascimento.get(Calendar.MONTH)) {
			age--;
		} else {
			if (atual.get(Calendar.MONTH) == dtnascimento.get(Calendar.MONTH)
					&& atual.get(Calendar.DAY_OF_MONTH) < dtnascimento.get(Calendar.DAY_OF_MONTH)) {
				age--;
			}
		}

		return age;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDtnascimento() {
		return dtnascimento;
	}

	public void setDtnascimento(String dtnascimento) {
		this.dtnascimento = dtnascimento;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public String toString() {
		return "Nome: " + getNome() + "\nData de nascimento: " + getDtnascimento() + "\nAltura: " + getAltura();
	}

}
