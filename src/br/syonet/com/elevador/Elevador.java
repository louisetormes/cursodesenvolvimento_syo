package br.syonet.com.elevador;

public class Elevador extends Predio {
	
	public int capacidadetotal;
	public int pessoas;
	
	public Elevador(int capacidadetotal, int pessoas, int totalandares, int andaratual) {
		super(totalandares, andaratual);
		this.capacidadetotal = capacidadetotal;
		this.pessoas = pessoas;
	}

	public int getCapacidadetotal() {
		return capacidadetotal;
	}
	
	public void setCapacidadetotal(int capacidadetotal) {
		this.capacidadetotal = capacidadetotal;
	}
	
	public int getPessoas() {
		return pessoas;
	}
	
	public void setPessoas(int pessoas) {
		this.pessoas = pessoas;
	}
	
	public void inicializa(int capacidadetotal, int totalandares) {
		this.capacidadetotal = capacidadetotal;
		this.totalandares = totalandares;
	}
	
	public void entraPessoa() {
		this.pessoas+=1;
	}
	
	public void saiPessoa() {
		this.pessoas-=1;
	}
	
	public void sobeAndar() {
		this.andaratual+=1;
	}
	
	public void desceAndar() {
		this.andaratual-=1;
	}
}
