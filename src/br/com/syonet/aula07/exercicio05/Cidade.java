package br.com.syonet.aula07.exercicio05;

	public class Cidade {

		private String cidade;
		private UfEnum uf;
		
		public Cidade(String cidade, UfEnum uf) {
			super();
			this.cidade = cidade;
			this.uf = uf;
		}
		
		public String toString() {
			return  "Cidade: " + cidade + "\nUF:"+ uf;
		}

		public void setNomeCidade(String cidade) {
			this.cidade = cidade;
		}

		public void setEstado(UfEnum uf) {
			this.uf = uf;
		}

		public String getNomeCidade() {
			return cidade;
		}

		public UfEnum getUf() {
			return uf;
		}
		
		
		

}
