package br.syonet.com.mensagens;
//CLASSES: Sms e Email

//herd�o: Nome, telefone e e-mail

public class Contato {

	public String nome;
	// public String email;
	public String mensagem;

	public Contato() {
		super();
	}

	public Contato(String nome, String mensagem) {
		this.nome = nome;
		this.mensagem = mensagem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
