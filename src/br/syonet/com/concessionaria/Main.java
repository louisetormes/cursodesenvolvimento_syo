package br.syonet.com.concessionaria;

import java.util.ArrayList;
import java.util.Scanner;

import br.syonet.com.imobiliaria.Imovel;

public class Main {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Concessionaria c = new Concessionaria(0, 0, 0, null, 0, null, null, null, 0, null);
		Caminhao caminhao = new Caminhao(0, 0, 0, null, 0, null, null, null, 0, null);
		Moto m = new Moto(0, 0, 0, null, 0, null, null, null, 0, null);
		Concessionaria listConcessionaria[] = new Concessionaria[1];
		listConcessionaria[0] = new Concessionaria(0, 0, 0, null, 0, null, null, null, 0, null);
		Caminhao listCaminhao[] = new Caminhao[1];
		listCaminhao[0] = new Caminhao(0, 0, 0, null, 0, null, null, null, 0, null);
		Moto listMoto[] = new Moto[1];
		listMoto[0] = new Moto(0, 0, 0, null, 0, null, null, null, 0, null);
		
		System.out.println("Bem vindo! \nInclua um ve�culo:");
		c.incluiveiculo = entrada.nextInt();
		listConcessionaria[0].incluindoVeiculo();
		
		System.out.println("Atualize o valor de venda do ve�culo:");
		c.valorvenda = entrada.nextDouble();
		listConcessionaria[0].alteraValor();
		
		System.out.println("Selecione o modelo do ve�culo que deseja visualizar (NOVOS, SEMINOVOS OU AMBOS): ");
		c.modelo = entrada.next();
		listConcessionaria[0].selecionaTipo(null);
		
		System.out.println("Selecione a marca da Moto:");
		m.marca = entrada.next();
		listMoto[0].motoporMarca();
		
		System.out.println("Selecione as Cilindradas da Moto:");
		m.cilindradas = entrada.nextInt();
		listMoto[0].selecionaCilindradas();
	
		System.out.println("Selecione o tipo de carroceria que deseja visualizar:");
		caminhao.tipocarroceria = entrada.next();
		listCaminhao[0].selecionaCarroceria(null);
		
		System.out.println("Verifique se o cliente possui desconto:");
		c.tipocliente = entrada.toString();
		listConcessionaria[0].selecionaCliente(null);
		
		
		
	}

}
