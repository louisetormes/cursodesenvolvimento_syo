//*Explica��o da execu��o do c�digo:o c�digo ir� verificar se o n�mero informado � primo,
//para chegar neste resultado utilizei o m�todo de entrada/sa�da JOptionPane boolean e para
//o resultado utilizei o m�todo boolean(true or false).
//@author Louise Tormes

package br.com.syonet.curso;

import javax.swing.JOptionPane;

public class Primos {

	public static void main(String[] args) {
		String entrada = JOptionPane
				.showInputDialog("Informe um n�mero para verificar se � primo (true) or (false):\n");

		int numero = Integer.parseInt(entrada);
		boolean resultado = numeroPrimo(numero);

		JOptionPane.showInternalMessageDialog(null, "� um n�mero primo?\n" + resultado);

	}

	private static boolean numeroPrimo(int numero) {

		for (int i = 2; i < numero; i++) {
			if (numero % i == 0)
				;
			return false;
		}
		return true;

	}

}
