package br.syonet.com.mensagens;

public class Email extends Contato {

	public String emaildestinatario;
	public String emailremetente;
	public String nomedestinatario;

	public Email(String nome, String mensagem) {
		super(nome, mensagem);
	}

	public Email(String emaildestinatario, String emailremetente, String nomedestinatario) {
		this.emaildestinatario = emaildestinatario;
		this.emailremetente = emailremetente;
		this.nomedestinatario = nomedestinatario;
	}

	public String getEmaildestinatario() {
		return emaildestinatario;
	}

	public void setEmaildestinatario(String emaildestinatario) {
		this.emaildestinatario = emaildestinatario;
	}

	public String getEmailremetente() {
		return emailremetente;
	}

	public void setEmailremetente(String emailremetente) {
		this.emailremetente = emailremetente;
	}

	public String getNomedestinatario() {
		return nomedestinatario;
	}

	public void setNomedestinatario(String nomedestinatario) {
		this.nomedestinatario = nomedestinatario;
	}

	public String toString() {
		return "Email:\n" + "Email destinatario - nome do destinatario: " + getEmaildestinatario() + " - "
				+ getNomedestinatario() + "\nEmail remetente - nome remetente: " + getEmailremetente() + " - "
				+ getNome() + "\n" + getMensagem();
	}

}
