package br.syonet.com.mensagens;

public class Sms extends Contato {

	public String telefone;

	public Sms(String nome, String mensagem) {
		super(nome, mensagem);
	}

	public Sms(String telefone) {
		this.telefone = telefone;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String toString() {
		return "Sms:\n" + getNome() + ": " + getMensagem() + "  " + getTelefone() + "\n";
	}

}
