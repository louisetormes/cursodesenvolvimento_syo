package br.syonet.com.imobiliaria;
//Exercicio Imobiliaria: para resolu��o deste exerc�cio criei primeiramente a classe Imovel,
//na qual inclui os valores principais que seriam herdados pelas demais classes, sendo esses:
//Valor do imovel (variavel valorvenda) e Area do imovel (variavel areaimovel), criei os metodos
//get e set de cada uma apos isso inclui o toString no final da classe.Foram criadas as demais classes
//(casas, apartamentos e terrenos), onde essas herd�o da classe imobiliaria seus metodos.Criei
//em cada uma seus atributos particulares (casas=n�mero de comodos e �rea do terreno,
//apartamentos=n�mero de comodos e sal�o de festas). Para a execu��o do c�digo foi criada a classe 
//CadastroImobiliaria, na qual temos ent�o os valores das vari�veis solicitadas neste exercicio.
//Para listar os dados das classes, foi utilizado um arraylist e utilizado o for each 
//para a execu��o do array.

import java.util.ArrayList;

public class CadastroImobiliaria {

	public static void main(String[] args) {
		ArrayList<Imovel> listaImoveis = new ArrayList<>();
		Terreno t = new Terreno(0, 0);
		Casa c = new Casa(0);
		Apartamento a = new Apartamento(true);
	
		c.setAreaimovel(17.300F);
		c.setQuantcomodos(9);
		c.setValorvenda(450.000F);
		c.setAreaterreno(20.300F);

		a.setAreaimovel(12.000F);
		a.setQuantcomodos(7);
		a.setValorvenda(250.000F);
		a.setSalaofestas(true);
		
		t.setAreaimovel(28.030F);
		t.setValorvenda(180.000F);

		
		
		listaImoveis.add(c);
		listaImoveis.add(a);
		listaImoveis.add(t);

		for (Imovel listaImovel : listaImoveis) {
			System.out.println(listaImovel);
		}

	}

}
