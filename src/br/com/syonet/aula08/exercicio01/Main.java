package br.com.syonet.aula08.exercicio01;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		CaracteristicaVeiculo veiculo1 = new CaracteristicaVeiculo("VW", "FOX", "IQC-7D63", "740A9CT9L59606", 30.000);
		CaracteristicaVeiculo veiculo2 = new CaracteristicaVeiculo("VW", "JETTA", "IUA-2089", "200B7RS8G41512", 27.000);
		CaracteristicaVeiculo veiculo3 = new CaracteristicaVeiculo("VW", "SPACECROSS", "ABC-2147", "570D8BC8G41512", 40.000);
		CaracteristicaVeiculo veiculo4 = new CaracteristicaVeiculo("VW", "T-CROSS", "DEF-1115", "440F8JG6J12707", 70.000);
		CaracteristicaVeiculo veiculo5 = new CaracteristicaVeiculo("FIAT", "UNO", "DEF-1110", "440F8KLPJ156707", 21.000);
		List<CaracteristicaVeiculo> listaVeiculo = Arrays.asList(veiculo1, veiculo2, veiculo3, veiculo4, veiculo5);
		System.out.println("\n-------------------\nVe�culos cadastrados no sistema: \n");
		listaVeiculo.forEach(System.out::println);
		Double somaVeiculo = valorTotal(listaVeiculo);
		List<CaracteristicaVeiculo> ordemValor = listarValores(listaVeiculo);

		System.out.println("\n-------------------\nRetorna ve�culos da marca VW: \n");
		listaVeiculo.stream()
		.filter(v -> v.getMarca() == "VW")
		.map(v -> "Ve�culo da marca VW: " + v.getChassi())
		.forEach(System.out::println);
		
		System.out.println("\n-------------------\nVe�culos de Modelo GOL: \n");
		listaVeiculo.stream()
		.filter(v -> v.getModelo() != "GOL")
		.map(v -> "N�o � modelo Gol encontrado! ")
		.forEach(System.out::println);
		
		System.out.println("\n-------------------\nVe�culos de Modelo JETTA: \n");
		listaVeiculo.stream()
		.filter(v -> v.getModelo() == "JETTA")
		.map(v -> "Ve�culo de Modelo Jetta encontrado: " + v.getChassi())
		.forEach(System.out::println);
		
		System.out.println("\n-------------------\nPlacas cadastradas no sistema: \n");
		listaVeiculo.stream()
		.filter(v -> v.getPlaca() != null)
		.map(v -> "Placa: " +  v.getPlaca())
		.forEach(System.out::println);
		
		System.out.println("\n-------------------\nOrdem de valores: \n" + ordemValor);
		
		System.out.println("\\n-------------------\\nDuplica Valores: \n" + duplicaVeiculo(listaVeiculo));
		
		System.out.println("\n-------------------\nAltera placas para ABC1234: \n");
		listaVeiculo.stream()
		.peek(placa -> placa.setPlaca("ABC1234"))
		.forEach(System.out::println);
		
		
		
		System.out.println("Valor Total: " + somaVeiculo);
		
	
		
		
	}
		//soma
		public static Double valorTotal(List<CaracteristicaVeiculo> listaVeiculo) {
			return listaVeiculo.stream()
					.filter(v -> v.getValor() != 0)
					.map(v -> v.getValor())
					.reduce(0.0, Double::sum);
					
	

		}
		public static List<CaracteristicaVeiculo> duplicaVeiculo(List<CaracteristicaVeiculo> listaVeiculo) {
			return listaVeiculo.stream()
					.filter(v -> v != null)
					.flatMap(v -> Stream.of(v, v))
					.collect(Collectors.toList());

			
		

		}
		
		public static List<CaracteristicaVeiculo> listarValores(List<CaracteristicaVeiculo> listaVeiculo) {
			return listaVeiculo.stream()
					.filter(v -> v!=null)
					.sorted((v1, v2) -> v1
					.compareTo(v2))
					.collect(Collectors.toList());

		}

	
	
			
	
	
	
	}	
	


