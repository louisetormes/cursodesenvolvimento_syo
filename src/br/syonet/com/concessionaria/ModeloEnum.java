package br.syonet.com.concessionaria;

public enum ModeloEnum {
	
	NOVO("Novo"),
	SEMINOVO("Seminovo"),
	AMBOS("Ambos");
	
public String modelo;
	
	ModeloEnum(String modelo) {
		this.modelo = modelo;
	}
	public String getModelo() {
		return modelo;
	}

}
