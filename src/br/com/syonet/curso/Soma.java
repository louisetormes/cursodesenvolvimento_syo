//*Explica��o da execu��o do c�digo:O c�digo ir� somar de 0 at� o valor informado pelo usu�rio, 
//ap�s obter este resultado o mesmo ir� informar a quantidade de n�meros pares e �mpares
//deste resultado.Para chegar nesse c�digo realizei a inclus�o do joptionpane para a entrada e
//sa�da dos dados num�ricos, solicitando o valor ao usu�rio.Declarei a vari�vel de entrada 
//e as vari�veis de sa�da para os n�meros pares e impares.Para a somat�ria dos n�meros
//utilizei o m�todo static para ambos, realizando um la�o com o for e indicando com if 
//else, se par ou impar;
//@author Louise Tormes

package br.com.syonet.curso;

import javax.swing.JOptionPane;

public class Soma {

	public static void main(String[] args) {
		String numero = JOptionPane.showInputDialog("Informe um valor para ser somado:\n");
		int numerousuario = Integer.parseInt(numero);
		int somaresultado = somaNumero(numerousuario);
		int par = numeroPar(somaresultado);
		int impar = numeroImpar(somaresultado);

		JOptionPane.showMessageDialog(null, "Total da soma do n�mero informado �:\n" + somaresultado);
		JOptionPane.showMessageDialog(null, "Quantidade de n�meros pares:\n" + par);
		JOptionPane.showMessageDialog(null, "Quantidade de n�meros impares:\n" + impar);

	}

	static int somaNumero(int numerousuario) {
		int soma = 0;
		for (int i = 1; i <= numerousuario; i++) {
			soma += i;
		}
		return soma;
	}

	static int numeroPar(int somaresultado) {
		int quantPar = 0;
		for (int i = 1; i <= somaresultado; i++) {
			if (i % 2 == 0) {
				continue;
			} else
				quantPar += 1;

		}

		return quantPar;
	}

	static int numeroImpar(int somaresultado) {
		int quantImpar = 0;
		for (int i = 1; i <= somaresultado; i++) {
			if (i % 2 == 1) {
				continue;
			} else
				quantImpar += 1;

		}
		return quantImpar;
	}
}
