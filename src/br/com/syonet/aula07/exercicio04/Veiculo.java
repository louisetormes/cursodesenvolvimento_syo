package br.com.syonet.aula07.exercicio04;

public class Veiculo {

private String marca;
private String modelo;
private String chassi;

public Veiculo marca(String marca) {
	this.marca = marca;
	return this;
}

public Veiculo modelo(String modelo) {
	this.modelo = modelo;
	return this;
}

public Veiculo chassi(String chassi) {
	this.chassi = chassi;
	return this;
}

public DadosVeiculo buid() {
	return new DadosVeiculo(marca, modelo, chassi);
}
}

