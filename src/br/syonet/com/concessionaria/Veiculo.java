package br.syonet.com.concessionaria;

public class Veiculo {

	public int incluiveiculo;
	public int quantrodas;
	public double peso;
	public String cor;
	public int ano;
	public String marca;
	public ModeloEnum modelo;
	public TipoVeiculoEnum tipoveiculo;
	public double valorvenda;
	private TipoClienteEnum tipocliente;
	
	public Veiculo(int incluiveiculo, int quantrodas, double peso, String cor, int ano, String marca, 
			ModeloEnum modelo, TipoVeiculoEnum tipoveiculo, double valorvenda, TipoClienteEnum tipocliente) {
		this.incluiveiculo = incluiveiculo;
		this.quantrodas = quantrodas;
		this.peso = peso;
		this.cor = cor;
		this.ano = ano;
		this.marca = marca;
		this.modelo = modelo;
		this.tipoveiculo = tipoveiculo;
		this.valorvenda = valorvenda;
		this.tipocliente = tipocliente;
		}
	

	public int getIncluiveiculo() {
		return incluiveiculo;
	}
	
	public void setIncluiveiculo(int incluiveiculo) {
		this.incluiveiculo = incluiveiculo;
	}
	
	public int getQuantrodas() {
		return quantrodas;
	}
	
	public void setQuantrodas(int quantrodas) {
		this.quantrodas = quantrodas;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	public String getCor() {
		return cor;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	public int getAno() {
		return ano;
	}
	
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public ModeloEnum getModelo() {
		return modelo;
	}
	
	public void setModelo(ModeloEnum modelo) {
		this.modelo = modelo;
	}
	
	public TipoVeiculoEnum getTipoveiculo() {
		return tipoveiculo;
	}
	
	public void setTipoveiculo(TipoVeiculoEnum tipoveiculo) {
		this.tipoveiculo = tipoveiculo;
	}
	
	public double getValorvenda() {
		return valorvenda;
	}
	
	public void setValorvenda(double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	public TipoClienteEnum getTipocliente() {
		return tipocliente;
	}
	
	public void setTipocliente(TipoClienteEnum tipocliente) {
		this.tipocliente = tipocliente;
	}
	
}
	
//	public void alteraValor() {
//		this.valorvenda+=1;
	

