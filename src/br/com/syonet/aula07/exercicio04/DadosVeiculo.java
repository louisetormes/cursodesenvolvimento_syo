package br.com.syonet.aula07.exercicio04;

import java.util.List;
import java.util.Optional;

public class DadosVeiculo {

	private String marca;
	private String modelo;
	private String chassi;

	public DadosVeiculo(String marca, String modelo, String chassi) {
		this.marca = marca;
		this.modelo = modelo;
		this.chassi = chassi;
	}

	public String toString() {
		return "\nMarca: " + marca +  "\nModelo: " + modelo + "\nChassi: " + chassi;
	}

	public static Optional<DadosVeiculo> encontrarVeiculoChassi(List<DadosVeiculo> veiculos, String chassi) {
		Optional<DadosVeiculo> veiculo =  veiculos.stream().filter(v -> v.getChassi().get().equals(chassi)).findAny();
		if(veiculo.isPresent()) {
			return Optional.ofNullable(veiculo).get();
		}
		else {
			return Optional.empty();
		}
	}

	public Optional<String> getChassi() {
		return Optional.ofNullable(chassi);
	}

}

