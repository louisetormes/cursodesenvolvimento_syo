package br.syonet.com.concessionaria;

public enum TipoVeiculoEnum {

	CARRO("Carro"),
	MOTO("Moto"),
	CAMINHAO("Caminhao");
	
	
	public String tipoveiculo;
	
	TipoVeiculoEnum(String tipoveiculo) {
		this.tipoveiculo = tipoveiculo;
	}
	public String getTipoveiculo() {
		return tipoveiculo;
	}
}
