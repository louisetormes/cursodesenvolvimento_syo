package br.syonet.com.concessionaria;

public enum TipoClienteEnum {
	
	PESSOAFISICA("Pessoa F�sica"),
	PESSOAJURIDICA("Pessoa Jur�dica"),
	PCD("PCD"),
	AGRICULTOR("Agricultor");
	
public String tipocliente;
	
	TipoClienteEnum(String tipocliente) {
		this.tipocliente = tipocliente;
	}
	
	public String getTipocliente() {
		return tipocliente;
	}
	
	

}
