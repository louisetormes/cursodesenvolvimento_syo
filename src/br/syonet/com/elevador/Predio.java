package br.syonet.com.elevador;

public abstract class Predio {

	public int totalandares;
	public int andaratual;

	public Predio(int totalandares, int andaratual) {
		super();
		this.totalandares = totalandares;
		this.andaratual = andaratual;
	}

	public int getTotalandares() {
		return totalandares;
	}

	public void setTotalandares(int totalandares) {
		this.totalandares = totalandares;

	}
	
	public int getAndaratual() {
		return totalandares;
	}

	public void setAndaratual(int andaratual) {
		this.andaratual = andaratual;

	}

}
