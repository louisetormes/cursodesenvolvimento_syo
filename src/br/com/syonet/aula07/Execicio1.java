package br.com.syonet.aula07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Execicio1 {

	public static void main(String[] args) {
		
		System.out.println("Ordena os elementos da lista:");
		List<Integer> numbers = new ArrayList<>();
		
		numbers.add(2);
		numbers.add(1);
		numbers.add(3);
		numbers.add(5);
		numbers.add(6);
		numbers.add(4);
		numbers.add(7);
		
		numbers.stream().sorted().collect(Collectors.toList()).forEach(number -> System.out.println(number));
	}
		
	}

