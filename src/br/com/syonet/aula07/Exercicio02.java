package br.com.syonet.aula07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Exercicio02 {

	public static void main(String[] args) {

		List<String> Nomes = new ArrayList<String>();

		Nomes.add("Louise");
		Nomes.add("Maria");
		Nomes.add("Pedro");
		Nomes.add("Lucas\n");
		
		Nomes.forEach(System.out::println);

		System.out.println("Nomes com o Car�cter L:\n");

		Nomes = procuraCaracter(Nomes, 'L');
		Nomes.forEach(System.out::println);
	}

	static List<String> procuraCaracter(List<String>nomes, char primeiroCaracter){
		List<String> achaCaracter = nomes
						.stream()
						.filter(nome -> nome.charAt(0)==primeiroCaracter)
						.sorted()
						.collect(Collectors.toList());
		return achaCaracter;
	}

}
